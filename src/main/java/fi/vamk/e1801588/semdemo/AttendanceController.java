package fi.vamk.e1801588.semdemo;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class AttendanceController {
    @Autowired
    private AttendanceRepository repository;

    @GetMapping("/attendances")
    public Iterable<Attendance> list() {
        return repository.findAll();
    }

    @GetMapping("/attendance/{id}")
    public Optional<Attendance> get(@PathVariable("id") int id) {
        return repository.findById(id);
    }

    @PostMapping("/attendance")
    public @ResponseBody Attendance create(@RequestBody Attendance item) {
        return (item.getDate() != null) ? repository.save(item) : null;
    }

    @PutMapping("/attendance")
    public @ResponseBody Attendance update(@RequestBody Attendance item) {
        return (item.getDate() != null) ? repository.save(item) : null;
    }

    @DeleteMapping("/attendance")
    public void date(@RequestBody Attendance item) {
        repository.delete(item);
    }

}