package fi.vamk.e1801588.semdemo;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer> {
    public Attendance findByKey(String key);
    public Attendance findByDate(Date date);
}