package fi.vamk.e1801588.semdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SemdemoApplication {
	@Autowired
	private AttendanceRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(SemdemoApplication.class, args);
	}

	@Bean
	public void initDate() {
		Attendance att = new Attendance("QWERTY");
		Attendance att2 = new Attendance("ABC");
		Attendance att3 = new Attendance("XYZ");
		repository.save(att);
		repository.save(att2);
		repository.save(att3);
	}

}
