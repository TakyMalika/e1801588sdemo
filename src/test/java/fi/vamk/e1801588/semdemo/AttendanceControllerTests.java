package fi.vamk.e1801588.semdemo;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.apache.commons.collections4.IterableUtils;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = { "fi.vamk.e1801588.semdemo" })
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)
public class AttendanceControllerTests {
    private static final Date date = null;
    @Autowired
    private AttendanceRepository repository;

    /*
     * Testcase 1 The solution can save, fetch by key and delete the attendance
     * 
     */
    @Test
    public void postGetDeleteAttendance() {
        Iterable<Attendance> begin = repository.findAll();
        // System.out.println(IterableUtils.size(begin));
        // given
        Attendance att = new Attendance("ABCD");
        System.out.println( "ATT: " + att.toString());
        // test save
        Attendance saved = repository.save(att);
        System.out.println("SAVED: " + saved.toString());
        // when
        Attendance found = repository.findByKey(att.getKey());
        System.out.println("FOUND: " + found.toString());
        // then
        assertThat(found.getKey()).isEqualTo(att.getKey());
        repository.delete(found);
        Iterable<Attendance> end = repository.findAll();
        // System.out.println(IterableUtils.size(end));
        assertEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));
    }

    /*
     * Testcase 2 The solution can save attendance with key and date and 
     * fetch by only date
     */
    @Test
    public void addAttendance() {
        // given
        Attendance att = new Attendance("A1B2", date);
        System.out.println( "ATTENDANCE: " + att.toString());
        // test save
        Attendance saved = repository.save(att);
        System.out.println("SAVED_ATTENDANCE: " + saved.toString());
        // when
        // Attendance found = repository.findByDate(att.getDate());
        // System.out.println("FOUND_ATTENDANCE: " + found.toString());
        // // then
        // assertThat(found.toString()).isEqualTo(att.toString());
    }
    
}